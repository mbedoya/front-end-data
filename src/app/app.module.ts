import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFireAuthModule } from "angularfire2/auth";
import { AppComponent } from './app.component';

export const firebaseConfig = {
  apiKey: "AIzaSyCjowXt8VjJRFKPXEY2swYs74vhl7IHbYw",
  authDomain: "mungos-dev.firebaseapp.com",
  databaseURL: "https://mungos-dev.firebaseio.com",
  projectId: "mungos-dev",
  storageBucket: "mungos-dev.appspot.com",
  messagingSenderId: "676885951191"
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
