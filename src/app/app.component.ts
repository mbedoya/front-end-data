import { Component } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from "angularfire2/auth";
import { Observable } from 'rxjs/Observable';
import * as firebase from "firebase/app";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  user: Observable<firebase.User>;
  items: FirebaseListObservable<any[]>;
  products: FirebaseListObservable<any[]>;
  content: FirebaseListObservable<any[]>;
  msgVal: string = '';
  title = 'app';

  constructor(public afAuth: AngularFireAuth, public af: AngularFireDatabase) {
    this.items = af.list('/messages', {
      query: {
        limitToLast: 50
      }
    });

    this.products = af.list('/products', {
      query: {
        limitToLast: 50
      }
    });

    this.content = af.list('/content', {
      query: {
        limitToLast: 50
      }
    });

    this.user = this.afAuth.authState;
  }

  login() {
    this.afAuth.auth.signInAnonymously();
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  send() {
    this.items.push({ message: this.msgVal });

    this.af.list("/content").remove();

    this.content.push({
      id: 1,
      title: '¿Quiénes somos?',
      friendlyUrl: 'quienes-somos',
      description: 'Mungos S.A.S es una empresa dedicada a crear vínculos de felicidad entre las personas y sus mascotas.   Mungos S.A.S ofrece a los consumidores productos alimenticios innovadores con altos estándares de calidad e inocuidad,    elaborados especialmente para el cuidado de las mascota, generando nuevas experiencias y sensaciones en el paladar de las mismas'
    });

    this.content.push({
      id: 2,
      title: 'Distribuidores',
      friendlyUrl: 'distribuidores',
      description: 'Estos son nuestros distribuidores a nivel nacional',
      details: [
        { text: 'Distribuciones filia pets: Carrera 65 No 28-28 - Tel: 3663861- 3192099138 - Medellín Antioquia. Correo: distribucionesfiliapets@gmail.com - ZONA ANTIOUIA'},
        { text: 'Agropets : Cl. 40 Sur #30-59, Envigado, Antioquia -  2709944 - Medellín Antioquia. Correo: agropets1@une.net.co - ZONA ANTIOQUIA'},
        { text: 'Nutrieco  Carrera 47 # 66-35- Oficina 101, barranquilla -  Cel: 3017722536, Tel: (57-5)3585129  gerencia@nutrieco.net-  ZONA - ATLANTICO, MAGDALENA, SUCRE, BOLIVAR, CORDOBA, URABA.'}
      ]
    });

    this.content.push({
      id: 3,
      title: 'Contacto',
      friendlyUrl: 'contacto',
      description: 'Teléfono fijo (Medellín): (57)4 3663861 - Celular 319 2099138',
      details: []
    });

    this.af.list("/products").remove();

    this.products.push({
      id: 1,
      title: 'Mungos Cábanos',
      friendlyUrl: 'mungos-cabanos',
      description: 'Es un delicioso Suplemento alimenticio (Snack) blando, elaborado con carne fresca, que brinda nuevas sensaciones en el paladar de  la mascota. Formulado especialmente para perros de 6 meses en adelante',
      featuredImageUrl: '/assets/images/Cabanos_Medio.png',
      imageUrl: '/assets/images/Cabanos_Grande.png',
      categories: ["Perros", "Cábanos"],
      featured: true,
      references: [{
        title: 'Dog Pack',
        unitPrice: 2000
      },
      {
        title: '80 gramos',
        unitPrice: 4000
      }]
    });

    this.products.push({
      id: 2,
      title: 'Mungos Brownies',
      friendlyUrl: 'mungos-brownies',
      description: 'Deliciosos Brownies BLANDOS tipo repostería, que llenarán de alegría el paladar de tu mejor amigo.Formulado especialmente para perros de 5 meses en adelante.',
      featuredImageUrl: 'https://firebasestorage.googleapis.com/v0/b/mungos-dev.appspot.com/o/Brownies_Medio.png?alt=media&token=cbc7e8a0-0343-4e32-a35b-268c8b3a414e',
      imageUrl: 'https://firebasestorage.googleapis.com/v0/b/mungos-dev.appspot.com/o/Brownie%20Grande.png?alt=media&token=d31ebc10-6907-451e-9f60-c8010943f8fd',
      categories: ["Perros", "Brownies"],
      featured: true,
      references: [{
        title: 'Dog Pack',
        unitPrice: 2000
      },
      {
        title: '80 gramos',
        unitPrice: 4000
      }]
    });

    this.products.push({
      id: 3,
      title: 'Mungos Rollitos',
      friendlyUrl: 'mungos-rollitos',
      description: 'Deliciosos rollitos con relleno blando con sabor a carne y pollo, que brindará nuevas sensaciones en el paladar de tu peludo. Es apto para todas las razas. Formulado especialmente para perros de 5 meses en adelante. Alimento Funcional (Snack) Enriquecido con Probióticos , Prebióticos y Lecitina de soya que mejoran el Sistema digestivo de tu MASCOTA mejorando su salud intestinal. Sabores: Carne y pollo',
      extendedDescription: 'Galleta blanda – fácil de masticar. Prebióticos Ganoderma lucidum,: Salud intestinal pelo brillante.  Sabor insuperable. Lecitina de soya: Sistema nervioso saludable',
      featuredImageUrl: '/assets/images/Cabanos_Medio.png',
      imageUrl: 'https://firebasestorage.googleapis.com/v0/b/mungos-dev.appspot.com/o/Rollitos_Grande.png?alt=media&token=e829c20f-9c06-47e7-82f3-109300bacc8f',
      categories: ["Perros", "Rollitos"],
      featured: true,
      references: [{
        title: 'Dog Pack',
        unitPrice: 2000
      },
      {
        title: '80 gramos',
        unitPrice: 4000
      },
      {
        title: '200 gramos',
        unitPrice: 8000
      },
      {
        title: '500 gramos',
        unitPrice: 17000
      },
      {
        title: '1000 gramos',
        unitPrice: 30000
      }]
    });

    this.products.push({
      id: 4,
      title: 'Mungos frutas',
      friendlyUrl: 'mungos-frutas',
      description: 'Mungos frutas Delicioso bocadito tipo gourmet con sabor a frutas que llenaran de alegría el paladar de tu mejor amigo. Formulado especialmente para perros. Es apto para todas las razas. Para perros de 5 meses en adelante. SABOR INSUPERABLE                                                                                                               Alimento Funcional (Snack) Enriquecido con Ácidos Grasos, Probióticos , Prebióticos, Aminoácidos y Lecitina de soya que mejoran el metabolismo de tu peludo haciéndolo mas fuerte y sano.',
      extendedDescription: 'Sabores: Mango, Banano y Mora. Análisis garantizado: Humedad (Max) 16 %, Proteína bruta: (Min) 10 %, Extracto Etéreo (Min) 8 %, Fibra bruta (Max) 7 % Cenizas (Max) 4 %, Energía Metabolizable Kcal/EM 3439.',
      featuredImageUrl: '/assets/images/Cabanos_Medio.png',
      imageUrl: 'https://firebasestorage.googleapis.com/v0/b/mungos-dev.appspot.com/o/Frutas_Grande.png?alt=media&token=dce9ef54-b159-4e36-92d6-4725a7c72f2f',
      categories: ["Perros", "Frutas"],
      featured: true,
      references: [
      {
        title: '80 gramos',
        unitPrice: 4000
      },
      {
        title: '200 gramos',
        unitPrice: 7500
      },
      {
        title: '500 gramos',
        unitPrice: 17000
      },
      {
        title: '1000 gramos',
        unitPrice: 27000
      }]
    });

    this.msgVal = '';
  }

}
